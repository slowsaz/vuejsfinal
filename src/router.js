import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import User from './components/user/User.vue'
import UserCreate from './components/user/UserCreate.vue'
import UserEdit from './components/user/UserEdit.vue'
import UserDetail from './components/user/UserDetail.vue'
import Categorie from "./components/categorie/Categorie";
import CategorieCreate from "./components/categorie/CategorieCreate";
import CategorieEdit from "./components/categorie/CategorieEdit";
import CategorieDetail from "./components/categorie/CategorieDetail";
import Tag from "./components/tag/Tag";
import TagCreate from "./components/tag/TagCreate";
import TagEdit from "./components/tag/TagEdit";
import TagDetail from "./components/tag/TagDetail";
import Article from "./components/article/Article";
import ArticleCreate from "./components/article/ArticleCreate";
import ArticleEdit from "./components/article/ArticleEdit";
import ArticleDetail from "./components/article/ArticleDetail";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path:'/users',
      component: User
    },
    {
      path:'/users/create',
      component: UserCreate
    },
    {
      path:'/users/:id/edit',
      component: UserEdit,
      name: 'UserEdit'
    },
    {
      path:'/users/:id',
      component: UserDetail,
      name: 'UserDetail'
    },
    {
      path:'/categories',
      component: Categorie
    },
    {
      path:'/categories/create',
      component: CategorieCreate
    },
    {
      path:'/categories/:id/edit',
      component: CategorieEdit,
      name: 'CategorieEdit'
    },
    {
      path:'/categories/:id',
      component: CategorieDetail,
      name: 'CategorieDetail'
    },
    {
      path:'/tags',
      component: Tag
    },
    {
      path:'/tags/create',
      component: TagCreate
    },
    {
      path:'/tags/:id/edit',
      component: TagEdit,
      name: 'TagEdit'
    },
    {
      path:'/tags/:id',
      component: TagDetail,
      name: 'TagDetail'
    },
    {
      path:'/articles',
      component: Article
    },
    {
      path:'/articles/create',
      component: ArticleCreate
    },
    {
      path:'/articles/:id/edit',
      component: ArticleEdit,
      name: 'ArticleEdit'
    },
    {
      path:'/articles/:id',
      component: ArticleDetail,
      name: 'ArticleDetail'
    }
  ]
})
